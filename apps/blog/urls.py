from django.urls import path
from .views import AuthorApiList

urlpatterns = [
    path('list_author/', AuthorApiList.as_view())
]