from django.shortcuts import render
from rest_framework import generics
from .serializers import AuthorSerializer
from .models import Author

# Create your views here.
class AuthorApiList(generics.ListAPIView):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer